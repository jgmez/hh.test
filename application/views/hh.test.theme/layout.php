<!doctype html>
<html lang="en" data-bs-theme="auto">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
	<meta name="generator" content="Hugo 0.118.2">
	<title>HH.TEST</title>

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
		integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

	<meta name="theme-color" content="#712cf9">

	<link rel="stylesheet" href="<?= base_url() ?>assets/global.css?v=<?= date('YmdHis') ?>">

</head>

<body data-bs-theme="dark">

	<div class="container">
		<?= $children ?>
	</div>

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
		crossorigin="anonymous"></script>

	<script src="https://code.jquery.com/jquery-3.7.1.min.js"
		integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>

	<script>
		const API_BASE_URL = '<?= base_url() ?>api/';
	</script>
	<?php if ($view_name == 'weather'): ?>
		<script src="<?= base_url() ?>assets/weather.js?v=<?= date('YmdHis') ?>"></script>
	<?php endif; ?>
</body>

</html>