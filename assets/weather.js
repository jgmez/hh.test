$(document).ready(function () {

    $("#input-city").focus();

    $("#btn-clean").on("click", function () {
        reset_form();
    });

    $("#btn-weather").on("click", function () {

        const payload = {
            'city': $("#input-city").val(),
            'temp_selector': $('input[name=temp_selector]:checked', '#main-form').val()
        };
        button_loading_experience(this, 'wait');
        api_request('get_weather_information', payload, this);
    });

    function api_request(resource, payload, button) {
        toggle_alert('#error-space', 'hide');
        toggle_alert('#success-space', 'hide');

        $.ajax({
            url: API_BASE_URL + resource, type: "POST", dataType: "json", contentType: "application/json",
            data: JSON.stringify(payload),
            success: function (response) {
                button_loading_experience(button, 'normal');
                if (response.status == 0) { //errors in our backend
                    toggle_alert('#error-space', 'show', response.result.message);
                    return;
                }

                if (response.result.external_api_result.cod != 200) { //errors with the external api
                    toggle_alert('#error-space', 'show', response.result.external_api_result.message);
                    return;
                }

                const data = response.result.external_api_result;
                $("#result-city").text(data.name);
                $("#result-temperature").text(data.main.temp);
                $("#result-description").text(data.weather[0].description);
                $("#result-icon").html('<img width="50" src="http://openweathermap.org/img/w/' + data.weather[0].icon + '.png"></img>');

                toggle_alert('#success-space', 'show', 'We got a success response here!');
            },
            error: function (jqXHR, textStatus, error) {
                button_loading_experience(button, 'normal');
                toggle_alert('#error-space', 'show', "error: " + jqXHR.responseText);                
            }
        });
    }

    function reset_form() {
        $("#input-city").focus();
        $('#main-form').trigger("reset");
        $("#table-weather tbody tr td").text('-');
        toggle_alert('#error-space', 'hide');
        toggle_alert('#success-space', 'hide');
    }

    function button_loading_experience(button, type) {
        if (type == 'wait') {
            $(button).find('label').hide();
            $(button).find('span').show();
            $(button).prop('disabled', true);
        } else if (type == 'normal') {
            $(button).find('label').show();
            $(button).find('span').hide();
            $(button).prop('disabled', false);
        }
    }

    function toggle_alert(component, state, message) {
        if (state == 'show') {
            $(component).text(message).fadeIn('fast');
        } else if (state == 'hide') {
            $(component).text(message).fadeOut('fast');
        }
    }

    //we are just disabling the form submittion when enter
    $(document).on("keydown", ":input:not(textarea)", function (event) {
        return event.key != "Enter";
    });
});