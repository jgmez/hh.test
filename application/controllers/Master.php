<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master extends CI_Controller
{

	public function index()
	{
		$view = $this->load->view(MAIN_VISUAL_THEME . 'pages/weather', [], TRUE);
		$this->load->view(MAIN_VISUAL_THEME . 'layout', ['children' => $view, 'view_name' => 'weather']);
	}
}
