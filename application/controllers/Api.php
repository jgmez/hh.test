<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('curl');
	}

	public function get_weather_information()
	{
		//get data information from the client
		$request_body = file_get_contents('php://input');
		$request_data = json_decode($request_body);

		//validating fields comming from the client
		if (!$request_data->city) {
			$result = [
				'message' => 'Please provide a city'
			];
			$this->print(0, 200, $result);
			return;
		}

		//For temperature in Fahrenheit use units=imperial
		//For temperature in Celsius use units=metric

		$url = OPENWEATHERMAP_API_URL . 'weather?q=' . $request_data->city . '&units=' . $request_data->temp_selector . '&APPID=' . OPENWEATHERMAP_API_KEY;
		$result_json = $this->curl->get($url);

		//improving data presentation
		$result_arr = json_decode($result_json);

		$result_arr = $this->clean_weather_info($result_arr, $request_data); //this is just to beautify descriptions

		$result = [
			'input' => $request_data,
			'external_api_result' => $result_arr,
			'status' => 1
		];

		$this->print(1, 200, $result);

	}

	private function clean_weather_info($result_arr, $request_data) //this is just to beautify descriptions
	{
		if (isset($result_arr->message)) {
			$result_arr->message = ucfirst($result_arr->message);
		}

		if (isset($result_arr->weather[0]->description)) {
			$result_arr->weather[0]->description = ucfirst($result_arr->weather[0]->description);
		}

		if (isset($result_arr->main->temp)) {
			$result_arr->main->temp = $result_arr->main->temp . ' ' . ($request_data->temp_selector == 'imperial' ? '°F' : '°C');
		}

		return $result_arr;
	}

	private function print($status, $code = 200, $result = null)
	{
		header_remove(); 	// clear the old headers		
		http_response_code($code); // set the actual code		
		header("Cache-Control: no-transform,public,max-age=300,s-maxage=900"); // set the header to make sure cache is forced		
		header('Content-Type: application/json'); // treat this as json

		$http_status = array(
			200 => '200 OK',
			400 => '400 Bad Request',
			422 => 'Unprocessable Entity',
			500 => '500 Internal Server Error'
		);
		// ok, validation error, or failure
		header('Status: ' . $http_status[$code]);

		// return the encoded json
		echo json_encode(
			[
				'http_code' => $code,
				'result' => $result,
				'status' => $status
			]
		);

	}

}
