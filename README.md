
# Weather App Installation

I'm Juan, from Colombia, I wrote the web app for getting cities weather using the OpenWeather API.

Thinking on an easy installation for you I used Codeigniter version 3, a super light framework, also assuming you have Php 7.x at hand.

  

# Download

Clone the web app from Gitlab:

https://gitlab.com/jgmez/hh.test

Just move the hh.test folder to your webserver public folder.  

It needs Apache.

# Installation

Please go to /application/config/config.php and change the base url according to your server/domain

$config['base_url'] = 'http://localhost:3001/hh.test/';

  

Example:

  

$config['base_url'] = 'http://your.server/hh.test/';

  

For running the app you just go to

http://localhost:3001/hh.test/

  

http://your.server/hh.test/

 
  

# Code Structure

  

###  HTML/VIEWS

The app is built under the MVC pattern, the default controller is /application/controllers/Master.php to serve views, here we load the layout and the specific weather view.

  

layout => /application/views/hh.test.theme/layout.php

weather view => /application/views/hh.test.theme/pages/weather.php

  

Here we build the HTML in an orderer way

  

###  API Controller

We have the /application/controllers/Api.php controller that makes the requests to OpenWeather API and delivers the response to the client (front end)

  

We have these three main functions

  

public function get_weather_information() //connect with OpenWeather API

  

private function clean_weather_info($result_arr, $request_data) //this is just to beautify descriptions and send them better to the client(front end)

  

private function print($status, $code = 200, $result = null) //build the response and send it to the client (front end)

  

### CSS AND JSS

On /assets folder we have a global css file for customizing styles, however in the layout file we have refs to boostrap 5.3 using a CDN, we added it to have nice looking.

  

On /assets folders we also have the weather.js that is linked with the /application/views/hh.test.theme/pages/weather.php view, it handles the form functionality, for example cleaning the form, make the ajax request to the /application/controllers/Api.php controller, show data received or errors.

We used jQuery library.

  

###  Notes

Global constants added here:

/application/config/config.php

  

define('MAIN_VISUAL_THEME','hh.test.theme/');

define('OPENWEATHERMAP_API_KEY','xxx');

define('OPENWEATHERMAP_API_URL','https://api.openweathermap.org/data/2.5/');