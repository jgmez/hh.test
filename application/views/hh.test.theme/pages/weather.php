<div class="col-lg-4 col-xxl-4 my-5 mx-auto">
    <div class="d-grid gap-2">
        <form id="main-form">
            <div class="mb-3">
                <label for="input-city" class="form-label">Type a city</label>
                <input type="text" class="form-control" id="input-city" placeholder="e.g Miami">
            </div>
            <div class="mb-3">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="temp_selector" id="fahrenheit" checked="checked"
                        value="imperial">
                    <label class="form-check-label" for="fahrenheit">Fahrenheit</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="temp_selector" id="celsius" value="metric">
                    <label class="form-check-label" for="celsius">Celsius</label>
                </div>
            </div>
            <button id="btn-weather" class="btn btn-primary" type="button">
                <label>Get weather information</label>
                <span style="display:none" class="spinner-grow spinner-grow-sm hidden" aria-hidden="true"></span>
                <span style="display:none" role="status" class="hidden">Loading...</span>
            </button>

            <button id="btn-clean" class="btn btn-secondary" type="button">
                <label>Clean form</label>
            </button>
        </form>
    </div>
    
    <div class="d-grid gap-2 mt-4">
        <table id="table-weather" class="table">
            <thead>
                <tr>
                    <th scope="col">City</th>
                    <th scope="col">Temperature</th>
                    <th scope="col">Description</th>
                    <th scope="col">Icon</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td scope="row" id="result-city">-</td>
                    <td id="result-temperature">-</td>
                    <td id="result-description">-</td>
                    <td id="result-icon">-</td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="d-grid gap-2">
        <div id="error-space" style="display:none" class="alert alert-danger" role="alert"></div>
        <div id="success-space" style="display:none" class="alert alert-success" role="alert"></div>

    </div>
</div>